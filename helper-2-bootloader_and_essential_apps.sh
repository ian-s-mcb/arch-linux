##
## helper-2-bootloader_and_essential_apps.sh
##
## This script is invoked by the step-1-install.sh. That script is run
## as root once chrooted into new OS created by helper-1-base_os.sh.
##

# Time
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc

# Misc
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "rename-me" > /etc/hostname
echo "root:root" | chpasswd

# Install bootloader
pacman -S --noconfirm intel-ucode # Do this before installing bootloader
pacman -S --noconfirm grub efibootmgr os-prober
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg # takes at least 60 sec

# Install misc essential apps
# dosfstools - For Fat32 file system support
# exfat-utils - For ExFat file system support
# mdadm - For software RAID
# networkmanager - For networking
# ntfs-3g - For Microsoft NTFS file system support
# reflector - For pacman mirror picking
pacman -S --noconfirm dosfstools exfat-utils mdadm networkmanager ntfs-3g reflector
sudo systemctl enable NetworkManager

# Install basic convenience apps
pacman -S --noconfirm git openssh tmux vim
sudo systemctl enable sshd.service
sudo systemctl start sshd.service

# Upgrade all outdated packages
pacman -Su --noconfirm

# Users
echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
groupadd -r sudo
useradd -b /home -G sudo,video -m ian
echo 'ian:ian' | chpasswd ian
echo 'root:root' | chpasswd root

# Copy repo to OS for convenience
cp -r /arch-linux /home/ian
chown ian:ian -R /home/ian/arch-linux
