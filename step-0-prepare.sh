##
## step-0-prepare.sh
##
## This script is NOT to be run all at once. Instead, it is a guideline
## of commands to be run manually, as root, once booted into the USB
## installer.
##
wifi-menu
pacman -Sy
pacman -S --noconfirm git reflector
reflector -c US -f 10 -p https --verbose --save /etc/pacman.d/mirrorlist
git clone https://gitlab.com/ian-s-mcb/arch-linux ~/arch-linux
bash -x ~/arch-linux/step-1-install.sh
