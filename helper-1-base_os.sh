##
## helper-1-base_os.sh
##
## This script is invoked by the step-1-install.sh. That script is run
## as root once booted into the USB installer.
##

# Install OS
pacstrap -K /mnt base base-devel linux linux-firmware

# Filesystems
genfstab -U /mnt >> /mnt/etc/fstab
echo -e '# swapfile\n/mnt/storage/swap-arch none swap defaults 0 0' >> /mnt/etc/fstab
cat ~/arch-linux/assets/fstab-nas-additions >> /mnt/etc/fstab

# Locale
echo 'en_US.UTF-8 UTF-8' >> /mnt/etc/locale.gen

# Hosts
cp ~/arch-linux/assets/hosts /mnt/etc/hosts
