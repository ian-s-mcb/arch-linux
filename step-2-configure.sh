##
## step-2-configure.sh
##
## This script is to be run as regular user ian after booting into the
## newly installed OS.
##

# Sync pacman mirrors
sudo pacman -Sy

# Fix pacman package signature
sudo pacman-key --init
sudo pacman-key --populate archlinux

# Partitions, folders
sudo mkdir -p /opt/aur
sudo mkdir -p /mnt/nas/{music,pictures,other,videos}
sudo mkdir -p /mnt/storage/{best-part,books,music,edu}
sudo mkdir -p /mnt/storage/dev/{dev-current,dev-archived-selected}
sudo mkdir -p /mnt/storage/downloads/os_images/
sudo mkdir -p /mnt/storage/downloads/torrents/{in,}complete
sudo mkdir -p /mnt/storage/envs/{deno,docker,go,node,python,vagrant,vim,virtualbox}
sudo mkdir -p /mnt/storage/envs/rust/{rustup,cargo}
sudo mkdir -p /mnt/storage/games/games-current
sudo chown ian:ian -R /mnt/* /opt/aur
ln -s -t ~ /mnt/storage
ln -s -t ~ /mnt/storage/dev
ln -s -t ~ /mnt/storage/downloads
ln -s -t ~ /mnt/storage/music
ln -s -t ~ /mnt/nas/other/books/reading_list
git clone https://gitlab.com/ian-s-mcb/music-best-part.git /mnt/storage/best-part/music
git clone https://gitlab.com/ian-s-mcb/playlists.git /mnt/storage/playlists
## >>>   ---------------------------   <<<
## >>>                                 <<<
## >>>   For hooray workstation only   <<<
## >>>                                 <<<
## >>>   ---------------------------   <<<
#rm -r /mnt/storage/music
#unlink ~/music
#ln -s -t ~ /mnt/nas/music
#rm -rf /mnt/storage/best-part
#ln -s -t /mnt/storage /mnt/nas/other/best-part

# Basic dev tools
sudo pacman -S --noconfirm alacritty ranger bash-completion man-pages
git config --global user.email "ian@iansmcb.com"
git config --global user.name "Ian S. McBride"
git config --global user.username "ian-s-mcb"
git config --global init.defaultBranch "main"
git config --global pull.rebase "false"
git clone https://gitlab.com/ian-s-mcb/arch-linux.git ~/dev/dev-current/arch-linux
git clone https://gitlab.com/ian-s-mcb/scripts.git ~/dev/dev-current/scripts
git clone https://gitlab.com/ian-s-mcb/dotfiles.git ~/dev/dev-current/dotfiles
ln -s -T ~/dev/dev-current/dotfiles ~/.config
# Add sym links for dotfiles that need to reside outside of ~/.config
rm ~/.bash{rc,_logout,_profile}
mkdir -p ~/.ssh
ln -s -T ~/.config/.ssh_config ~/.ssh/config
ln -s -t ~ ~/.config/.bash_logout
ln -s -t ~ ~/.config/.bash_profile
ln -s -t ~ ~/.config/.bashrc
ln -s -t ~ ~/.config/.dircolors
ln -s -t ~ ~/.config/.vimrc
ln -s -t ~ ~/.config/.pam_environment
systemctl enable ssh-agent.service --user
systemctl start ssh-agent.service --user

# Fonts
sudo pacman -S --noconfirm noto-fonts-emoji noto-fonts-cjk terminus-font ttf-dejavu ttf-firacode-nerd
echo 'FONT=ter-v18n' | sudo tee /etc/vconsole.conf

# Tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
ln -s -t ~ ~/.config/.tmux.conf
mkdir ~/.tmuxp
ln -s -t ~/.tmuxp ~/.config/base.yaml
sudo pacman -S --noconfirm tmuxp

# Vim
ln -s -T /mnt/storage/envs/vim/ ~/.vim
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
# Open Vim, then run :PlugInstall
# Open a TypeScript file with Vim, then run :LspInstallServer

# Window manager and friends
sudo pacman -S --noconfirm gdk-pixbuf2 gnome-themes-extra grim libnotify mako network-manager-applet nm-connection-editor pipewire seatd slurp sway swaybg swayidle swaylock waybar wayland-protocols wf-recorder wl-clipboard wofi xdg-desktop-portal-wlr xorg-xwayland
cd /opt/aur
git clone https://aur.archlinux.org/light.git
git clone https://aur.archlinux.org/wlsunset-git.git
git clone https://aur.archlinux.org/wshowkeys-git.git
cd light
makepkg --noconfirm -si
cd ../wlsunset-git
makepkg --noconfirm -si
cd ../wshowkeys-git
makepkg --noconfirm -si
systemctl enable pipewire.service --user
systemctl start pipewire.service --user

# Audio
sudo pacman -S --noconfirm alsa-utils beets cuetools gst-plugins-bad gst-plugins-ugly pavucontrol pulseaudio pulseaudio-alsa pulseaudio-jack pulsemixer soundconverter
git clone https://aur.archlinux.org/shntool.git /opt/aur/shntool
cd /opt/aur/shntool
makepkg --noconfirm -si
# A reboot helped when pavucontrol couldn't connect to the pulsaudio
# daemon

# Bluetooth
sudo pacman -S --noconfirm blueman bluez bluez-utils pulseaudio-bluetooth
# Fix for Keychron keyboards
echo "options hid_apple fnmode=0" | sudo tee /etc/modprobe.d/hid_apple.conf
sudo mkinitcpio -P
sudo systemctl enable bluetooth.service
sudo systemctl start bluetooth.service

# Transmission-daemon
sudo pacman -S --noconfirm transmission-cli
# Add static copy of dotfile (to avoid ln causing the loss of strict file permissions)
sudo mkdir -p /var/lib/transmission/.config/transmission-daemon/
sudo cp ~/.config/transmission-daemon/settings.json /var/lib/transmission/.config/transmission-daemon/
sudo chown transmission:transmission -R /var/lib/transmission
sudo chown ian:transmission /mnt/storage/downloads/torrents/{in,}complete
chmod 770 /mnt/storage/downloads/torrents/{in,}complete
sudo systemctl start transmission
sudo systemctl enable transmission

# Web broswers
sudo pacman -S --noconfirm firefox chromium

# Mpd, mpc, ncmpcpp
sudo pacman -S --noconfirm mpd mpc ncmpcpp
ln -s -t ~/.config/mpd/ ~/music
ln -s -t ~/.config/mpd ~/storage/playlists
touch ~/.config/mpd/state
systemctl enable mpd.service --user
systemctl start mpd.service --user

# Misc OS config
sudo pacman -S --noconfirm nfs-utils # For NAS
echo 'HandleLidSwitch=ignore' | sudo tee -a /etc/systemd/logind.conf # For disabling suspend on laptop lid close
sudo pacman -S --noconfirm ntp
sudo systemctl enable ntpd.service
sudo systemctl start ntpd.service

# Misc apps
sudo pacman --noconfirm -S foliate neofetch
# Commented 2022-01-20 - Evince is unnecessary given Firefox
# git clone https://aur.archlinux.org/evince-light.git /opt/aur/evince-light
# cd /opt/aur/evince-light
# makepkg --noconfirm -si # Takes ~2 min
sudo pacman -S --noconfirm gimp mpv imv zip unzip unrar p7zip gucharmap curl wget img2pdf man parted ethtool wol
git clone https://aur.archlinux.org/slack-desktop.git /opt/aur/slack-desktop
cd /opt/aur/slack-desktop
makepkg --noconfirm -si
git clone https://aur.archlinux.org/insomnia-bin.git /opt/aur/insomnia-bin
cd /opt/aur/insomnia-bin
makepkg --noconfirm -si

# Android
sudo pacman -S --noconfirm android-tools android-udev

# Python
sudo pacman -S --noconfirm python-pip python-pipx
# The following exports are also in the .bashrc but step-2-configure.sh needs
# them here too, otherwise pipx places the installed apps in the wrong dir
export PIPX_HOME='/mnt/storage/envs/python'
export PIPX_BIN_DIR="$PIPX_HOME/bin"
export PIPX_MAN="$PIPX_HOME/share/man"
export PATH="$PATH:$PIPX_BIN_DIR"
pipx install stig ipython jupyterlab

# Commented 2021-12-02 - R is meh
# # R (the language)
# sudo pacman -S --noconfirm r tk openssl
# git clone https://aur.archlinux.org/rstudio-server-git.git /opt/aur/rstudio-server-git
# cd /opt/aur/rstudio-server-git
# makepkg --noconfirm -si # Takes +30 min

# Node.js
git clone https://aur.archlinux.org/nvm.git /opt/aur/nvm
cd /opt/aur/nvm
makepkg --noconfirm -si
mkdir -p /mnt/storage/envs/node
export NVM_DIR="/mnt/storage/envs/node"
source /usr/share/nvm/nvm.sh
source /usr/share/nvm/bash_completion
source /usr/share/nvm/install-nvm-exec
nvm install --lts
nvm use --lts
npm i -g vite npm-check-updates

# Deno
sudo pacman -S --noconfirm deno

# Rust
sudo pacman -S --noconfirm rustup
rustup default stable

# Go
sudo pacman -S --noconfirm go hugo

# Virtualization
sudo pacman -S --noconfirm docker docker-compose kubectl virtualbox vagrant virtualbox-host-modules-arch
# The following export prevents in the plugin installation step due to the
# version of the utility `date`
export VAGRANT_DISABLE_STRICT_DEPENDENCY_ENFORCEMENT=1
vagrant plugin install vagrant-vbguest
sudo usermod -a -G docker ian
sudo mkdir /etc/docker
echo -e "{\n  \"data-root\": \"/mnt/storage/envs/docker\"\n}" | sudo tee /etc/docker/daemon.json
