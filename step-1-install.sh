##
## step-1-install.sh
##
## This script is to be run as root once booted into the USB installer.
##

# Create partitions
if [ ! -f ~/arch-linux/helper-0-partitions.sh ]; then
  echo -e "Error: no helper-0-partitions.sh file found.\nCopy the example file or create one from scratch."
  exit 1
fi
bash -x ~/arch-linux/helper-0-partitions.sh

# Install base OS
bash -x ~/arch-linux/helper-1-base_os.sh

# Chroot into OS and install bootloader and essential apps
cp -r ~/arch-linux /mnt
arch-chroot /mnt ./arch-linux/helper-2-bootloader_and_essential_apps.sh

# Cleanup
rm -r /mnt/arch-linux/
