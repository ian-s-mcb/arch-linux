##
## step-3-manual_cleanup.sh
##
## This script is NOT to be run all at once. Instead, it is a guideline
## of commands to be run manually, as regular user ian, after booting
## into the newly installed OS.
##

# Hostname
sudo vim /etc/hostname
# - Add one line consisting of <HOSTNAME> 
sudo vim /etc/hosts
# - Update 3rd line to:
#     127.0.1.1       <HOSTNAME>.localdomain <HOSTNAME>
sudo hostnamectl hostname <HOSTNAME>

# SSH key
cp -r /path/to/ssh/folder/with/keys ~/.ssh

# Remove old copy of arch-linux repo
rm ~/arch-linux

# Replace repo remotes with ones that use ssh keys
cd ~/dev/dev-current/arch-linux
git remote set-url origin git@gitlab.com:ian-s-mcb/arch-linux.git
cd ~/dev/dev-current/dotfiles
git remote set-url origin git@gitlab.com:ian-s-mcb/dotfiles.git
cd ~/dev/dev-current/scripts
git remote set-url origin git@gitlab.com:ian-s-mcb/scripts.git
## >>>   ---------------------------   <<<
## >>>                                 <<<
## >>>   For non-hooray workstations   <<<
cd ~/storage/best-part/music/
git remote set-url origin git@gitlab.com:ian-s-mcb/best-part-music.git
## >>>                                 <<<
## >>>   ---------------------------   <<<

# Tmux
tmux
# Type `Ctrl-b I` to download plugins

# Bluetooth
vim /etc/bluetooth/main.conf
# - Under [General], add
#     DiscoverableTimeout = 0
# - Under [Policy], add
#     AutoEnable=true

# Shorten grub menu timeout, enable hibernate, enable software RAID
sudo vim /etc/default/grub
# - Set `GRUB_TIMEOUT=2`
# - Add `resume=UUID=<UUID_OF_SWAP_PARTITION> resume_offset=<OFFSET>` to line
#   beginning with GRUB_CMDLINE_LINUX_DEFAULT
# - <OFFSET> comes from running:
#   filefrag -v ~/storage/swap-arch | awk '$1=="0:" {print substr($4, 1, length($4)-2)}'
# - Set `GRUB_DISABLE_OS_PROBER=false`
sudo grub-mkconfig -o /boot/grub/grub.cfg # takes 3 sec
sudo vim /etc/mkinitcpio.conf
# - On line beginning with `HOOK=(base ...)`
#     - Add resume after udev, before fsck
#     - Add mdadm_udev after udev, before filesystems
sudo mkinitcpio -p linux

# Gimp
# Follow instructions to add arrow tool
# https://progsharing.blogspot.com/2018/06/draw-arrows-with-gimp-plugins.html#arrow_scm_branger
sudo cp ~/downloads/arrow.scm /usr/share/gimp/2.0/scripts/

# Hypervisors - VirtualBox
# Run GUI (VirtualBox), and in File > Preferences
# Set Default machine folder to ~/storage/envs/virtualbox

# Users
sudo passwd ian # Set a real password for ian
sudo visudo # Remove passwordless sudo for members of sudo group
sudo passwd -l root # Lock root user

# Pacman
sudo vim /etc/pacman.conf
# Uncomment: ParallelDownloads = 5

# Firefox - torrent magnet link handling
# Navigate to about:config
# Add a boolean
#    name: network.protocol-handler.expose.magnet
#    value: false
# Look up a magnet link, click it, associate scripts/add_magnet.sh to
# the type of link

# Firefox - HiDPI monitor
# Navigate to about:config
#   name: layout.css.devPixelsPerPx
#   value: 1.2
