# Arch Linux

A script that installs and configures an Arch Linux workstation to my
liking. This script references two external repos, one for [my
dotfiles][repo-dotfiles] and one for [my misc scripts][repo-scripts].

Important note: this script wipes an entire storage drive as part of the
file system partitioning step. Here is the the [partition layout
specified by the script](#partition-layout).

## How to use this script

* Create bootable USB OS installer (see
  [Arch Linux wiki page][wiki-installer] for details)
* Boot into the installer
* Run the following six commands after booting into the live OS
  installer. These same commands are in the file `step-0-prepare.sh`.

    ```bash
    # Connect to WiFi network
    wifi-menu
    pacman -Sy
    pacman -S --noconfirm git reflector
    reflector -c US -f 10 -p https --verbose --save /etc/pacman.d/mirrorlist
    git clone https://gitlab.com/ian-s-mcb/arch-linux ~/arch-linux
    bash -x ~/arch-linux/step-1-install.sh
    ```
* Reboot into the insalled OS (not the installer), login as ian, and run
  `bash -x ~/arch-linux/step-2-configure.sh`
* Inspect and selectively run commands in `step-3-manual_cleanup.sh`

## Disk usage

Disk capacity: 475 GiB

##### Partition layout
* 550 MB - esp
*   1 MB - grub_bios
*   5 GB - swap
*  30 GB - arch
*  30 GB - ubuntu
*  60 GB - win10
* 345 GB - storage

##### Disk usage milestones

* X.Y GB - <date> - base, base-devel

## Missing features

* Waybar tray icon for language input

[repo-dotfiles]: https://gitlab.com/ian-s-mcb/dotfiles
[repo-scripts]: https://gitlab.com/ian-s-mcb/scripts
[wiki-installer]: https://wiki.archlinux.org/index.php/USB_flash_installation_media
